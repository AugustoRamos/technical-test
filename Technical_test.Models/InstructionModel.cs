﻿namespace Technical_test.Models
{
    public class InstructionModel
    {
        public string Operator { get; set; }
        public double Number { get; set; }    
    }
}
