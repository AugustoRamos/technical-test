﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;

namespace Technical_test.Business.Helpers
{
    public class UpoladFileHelper
    {
        public static IEnumerable<string> UploadFile(HttpContext context, ref string message)
        {
            try
            {
                if (context.Request.QueryString["upload"] != null)
                {
                    var serverpath = HttpContext.Current.Server.MapPath("Upload");

                    var postedFile = context.Request.Files[0];

                    string file;

                    //For IE to get file name
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        var files = postedFile.FileName.Split('\\');
                        file = files[files.Length - 1];
                    }
                    else
                        file = postedFile.FileName;


                    if (!Directory.Exists(serverpath))
                        Directory.CreateDirectory(serverpath);

                    var fileDirectory = serverpath;
                    if (context.Request.QueryString["fileName"] != null)
                    {
                        file = context.Request.QueryString["fileName"];
                        if (File.Exists(fileDirectory + "\\" + file))
                        {
                            File.Delete(fileDirectory + "\\" + file);
                        }
                    }

                    var ext = Path.GetExtension(fileDirectory + "\\" + file);

                    if (!ext.Contains("txt"))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        message = "Extension " + ext + " is not allowed";
                        return null;
                    }

                    file = Guid.NewGuid() + ext;

                    fileDirectory = serverpath + "\\" + file;

                    postedFile.SaveAs(fileDirectory);

                    return File.ReadAllLines(fileDirectory);
                }
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                message = ex.Message;
                return null;
            }

            return null;
        }
    }
}
