﻿namespace Technical_test.Business.Interfaces
{
    public interface IOperatorService
    {
        double Subtract(double a, double b);
        double Add(double a, double b);
        double Multiply(double a, double b);
        double Divide(double a, double b);
    }
}
