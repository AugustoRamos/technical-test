﻿using System.Collections.Generic;
using System.Web;

namespace Technical_test.Business.Interfaces
{
    public interface ICalculateService
    {
        IList<string> ProccesFile(HttpContext context, ref string message);
    }
}
