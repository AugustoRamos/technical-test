﻿using Technical_test.Business.Interfaces;

namespace Technical_test.Business.Services
{
    public class OperatorService : IOperatorService
    {
        public double Subtract(double a, double b)
        {
            return a - b;
        }

        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Multiply(double a, double b)
        {
            return a * b;
        }

        public double Divide(double a, double b)
        {
            return a / b;
        }
    }
}
