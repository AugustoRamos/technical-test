﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Technical_test.Business.Helpers;
using Technical_test.Business.Interfaces;
using Technical_test.Models;

namespace Technical_test.Business.Services
{
    public class CalculateService : ICalculateService
    {
        private const string Apply = "APPLY";
        private const string Add = "ADD";
        private const string Multiply = "MULTIPLY";
        private const string Divide = "DIVIDE";
        private const string Subtract = "SUBTRACT";

        private readonly IOperatorService _operatorService;
        private readonly IList<InstructionModel> _instructions;
        private readonly IList<string> _response;

        public CalculateService()
        {
            _operatorService = new OperatorService();
            _response = new List<string>();
            _instructions = new List<InstructionModel>();
        }

        public IList<string> ProccesFile(HttpContext context, ref string message)
        {
            try
            {
                var fileLines = UpoladFileHelper.UploadFile(context, ref message);

                if (fileLines == null || !fileLines.Any())
                    return null;

                GetOperators(fileLines, ref message);
                Calculate(ref message);

                return _response;
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                message = ex.Message;
                return null;
            }
        }

        private void GetOperators(IEnumerable<string> lines, ref string message)
        {
            try
            {
                var validOperators = new List<string>
                {
                    Add,
                    Divide,
                    Multiply,
                    Subtract,
                    Apply
                };

                foreach (var line in lines)
                {
                    var instruction = line.Split(' ');
                    if (validOperators.Contains(instruction[0].ToUpper()))
                        _instructions.Add(new InstructionModel
                        {
                            Number = Convert.ToDouble(instruction[01]),
                            Operator = instruction[0].ToUpper()
                        });
                    else
                        message = "This line was ignored: " + line;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        private void Calculate(ref string message)
        {
            try
            {
                if (_instructions.FirstOrDefault(x => x.Operator.Contains(Apply)) == null)
                {
                    HttpContext.Current.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    message = "No line containing the 'Apply' instruction was found.";
                    return;
                }

                var applyNumber = _instructions.FirstOrDefault(x => x.Operator.Contains(Apply)).Number;
                double result = 0;


                for (var i = 0; i < _instructions.Count; i++)
                {
                    if (i > 0)
                        applyNumber = result;

                    switch (_instructions[i].Operator)
                    {
                        case Add:
                            _response.Add(applyNumber + " + " + _instructions[i].Number);
                            result = _operatorService.Add(applyNumber,_instructions[i].Number);
                            break;
                        case Multiply:
                            _response.Add(applyNumber + " * " + _instructions[i].Number);
                            result = _operatorService.Multiply(applyNumber, _instructions[i].Number);
                            break;
                        case Divide:
                            _response.Add(applyNumber + " / " + _instructions[i].Number);
                            result = _operatorService.Divide(applyNumber, _instructions[i].Number);
                            break;
                        case Subtract:
                            _response.Add(applyNumber + " - " + _instructions[i].Number);
                            result = _operatorService.Subtract(applyNumber, _instructions[i].Number);
                            break;
                    }
                }
                _response.Add("Result: " + result);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }
    }
}
