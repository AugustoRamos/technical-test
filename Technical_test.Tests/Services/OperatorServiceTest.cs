﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Technical_test.Business.Interfaces;
using Technical_test.Business.Services;

namespace Technical_test.Tests.Services
{
    [TestClass]
    public class OperatorServiceTest
    {
        private readonly IOperatorService _operatorService;

        public OperatorServiceTest()
        {
            _operatorService = new OperatorService();
        }

        [TestMethod]
        public void Subtract_Three_From_Three()
        {
            var result = _operatorService.Subtract(3, 3);
            const int expectedOutput = 0;

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod]
        public void Add_Three_To_Three()
        {
            var result = _operatorService.Add(3, 3);
            const int expectedOutput = 6;

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod]
        public void Multiply_Three_To_Three()
        {
            var result = _operatorService.Multiply(3, 3);
            const int expectedOutput = 9;

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod]
        public void Divide_Three_From_Three()
        {
            var result = _operatorService.Divide(3, 3);
            const int expectedOutput = 1;

            Assert.AreEqual(expectedOutput, result);
        }
    }
}
