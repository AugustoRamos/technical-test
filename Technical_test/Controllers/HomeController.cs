﻿using System.Linq;
using System.Web.Mvc;
using Technical_test.Business.Interfaces;
using Technical_test.Business.Services;

namespace Technical_test.Controllers
{
    public class HomeController : Controller
    {
        readonly ICalculateService _calculator = new CalculateService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Augusto Chaves Ramos";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Augusto Chaves Ramos";

            return View();
        }

        [HttpPost]
        public ActionResult ProccessFile()
        {
            var message = "";
            var response = _calculator.ProccesFile(System.Web.HttpContext.Current, ref message);
            var data = new { status = Response.StatusCode, result = response?.ToArray(), description = message };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}